import os
import tempfile
from numpy import ma
import streamlit as st
import time
from rag import ChatPDF

st.set_page_config(page_title="ChatPDF")


def display_messages():
    st.subheader("Chat")

    for message in st.session_state.chat_history:
        with st.chat_message(message["role"]):
            st.markdown(message["message"])

def process_input(user_input):
    if user_input and len(user_input.strip()) > 0:
        user_text = user_input.strip()
        user_message = {"role": "user", "message": user_text}
        st.session_state.chat_history.append(user_message)
        with st.chat_message("user"):
            st.markdown(user_text)

        with st.chat_message("assistant"):
            with st.spinner("Assistant is typing..."):
                agent_text = st.session_state["assistant"].ask(user_text)
            message_placeholder = st.empty()
            full_response = ""
            for chunk in agent_text.split():
                full_response += chunk + " "
                time.sleep(0.05)
                # Add a blinking cursor to simulate typing
                message_placeholder.markdown(full_response + "▌")
            message_placeholder.markdown(full_response)


        chatbot_message = {"role": "assistant", "message": agent_text}
        st.session_state.chat_history.append(chatbot_message)


def read_and_save_file():
    st.session_state["assistant"].clear()
    st.session_state["chat_history"] = [{"role": "assistant", "message": "How may I assist you today?"}]
    st.session_state["user_input"] = ""

    for file in st.session_state["file_uploader"]:
        with tempfile.NamedTemporaryFile(delete=False) as tf:
            tf.write(file.getbuffer())
            file_path = tf.name

        with st.session_state["ingestion_spinner"], st.spinner(f"Ingesting {file.name}"):
            st.session_state["assistant"].ingest(file_path)
        os.remove(file_path)


def page():
    if "chat_history" not in st.session_state:
        st.session_state["chat_history"] = [{"role": "assistant", "message": "How may I assist you today?"}]
    if "assistant" not in st.session_state:
        st.session_state["assistant"] = ChatPDF()

    st.header("ChatPDF")

    st.subheader("Upload a document")
    st.file_uploader(
        "Upload document",
        type=["pdf"],
        key="file_uploader",
        on_change=read_and_save_file,
        label_visibility="collapsed",
        accept_multiple_files=True,
    )

    st.session_state["ingestion_spinner"] = st.empty()

    display_messages()

    if prompt := st.chat_input():
        st.session_state.chat_history.append({"role": "user", "message": prompt})
        with st.chat_message("user"):
            st.write(prompt)


    if st.session_state.chat_history[-1]["role"] != "assistant":
        with st.chat_message("assistant"):
            with st.spinner("Assistant is typing..."):
                response = st.session_state["assistant"].ask(prompt)
            placeholder = st.empty()
            full_response = ''
            for chunk in response.split():  # Assume response is a string
                full_response += chunk + " "
                time.sleep(0.05)
                placeholder.markdown(full_response + "▌")
            placeholder.markdown(full_response)
        message = {"role": "assistant", "message": full_response}
        st.session_state.chat_history.append(message)




if __name__ == "__main__":
    page()
